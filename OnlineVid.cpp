#include <iostream>
#include <string>
#include "OnlineVid.h"

 using namespace std;

 OnlineVid::OnlineVid(){
	 views = 0;
	 id = "";
 }

 OnlineVid::OnlineVid(string name, unsigned vidViews)
 {
	id = name;
	views = vidViews;
 }

 OnlineVid::OnlineVid(unsigned vidViews, string name)
 {
	views = vidViews;
	id = name;
 }

 void OnlineVid::setId(string name)
 {
   id = name;
 }

 string OnlineVid::getId()
 {
   return id;
 }

 void OnlineVid::setViews(unsigned vidViews)
 {
   views = vidViews;
 }

 unsigned OnlineVid::getViews()
 {
  return views;
 }
