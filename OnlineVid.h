#ifndef ONLINEVID_H
#define ONLINEVID_H

#include <string>

using namespace std;

class OnlineVid

{
	private:  //properties
        	unsigned  views;
	        string id;       

	public:  //methods
        
		OnlineVid();//default constructor
		OnlineVid(string, unsigned);
		OnlineVid(unsigned, string);
        void setViews(unsigned);
	    unsigned getViews();
		void setId(string);
		string getId();

 };
 
 #endif

