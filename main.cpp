#include <iostream>
#include <string>
#include "OnlineVid.h"

using namespace std;

int main(){

	OnlineVid onlineVid1("Cats Doing Stuff", 1000546);
	OnlineVid onlineVid2("Dogs Sneezing", 25065420);	

	cout << endl;
	
	cout << "First video details: " << endl << endl;
	cout << "Video Name: " << onlineVid1.getId() << endl;
	cout << "Num. of Views: " << onlineVid1.getViews();
	cout << endl << endl;
	
	cout << "Second video details: "<< endl << endl;
	cout << "Video Name: " << onlineVid2.getId() << endl;
	cout << "Num. of Views: " << onlineVid2.getViews();
	cout << endl << endl;
	
	cout << "Changing first video details...";
	cout << endl << endl;
	
	onlineVid1.setId("Cat Doing Stuff Again");
	onlineVid1.setViews(1005002);
	
	cout << "New First video details: " << endl << endl;
	cout << "Video Name: " << onlineVid1.getId() << endl;
	cout << "Num. of Views: " << onlineVid1.getViews();
	cout << endl << endl;
	
	return 0;
	
}//End Main
